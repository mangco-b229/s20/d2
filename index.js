

// [SECTION] - While Loop
let count = 5;

while(count !== 0) {

	//First  iteration -> count == 5
	console.log("while: " + count)
	count--;
}


// [SECTION] - Do While Loop

/*
Syntax:
do{
	//code here
} while(condition)
*/

let number = Number(prompt("give me a number"))

do{
	console.log("Do While " + number)	
	number += 1
} while(number <  10) 
// [SECTION] - For loops

// for(let count = 0; count <= 20; count++) {
// 	let myString = "alex"
// }

let myString = "alex"
// console.log(myString[0])
// console.log(myString[1])
// console.log(myString[2])
// console.log(myString[3])

// for(let x = 0; x < myString.length; x++){
// 	console.log(myString[x])
// } 

let myName = "ALEx"

for(let i = 0; i < myName.length; i++) {
	// console.log(myName[i].toLowerCase())

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" ||
		myName[i].toLowerCase() == "e"
		) {
		console.log(3)
	} else {
		console.log(myName[i])
	}
}

// //[SECTION] - Continue and Break Statements

// for(let count = 0; count <= 20; count++) {
// 	// if remainder is equal to 0
// 	if(count % 2 === 0) {
// 		continue
// 	}

// 	if(count > 10) {
// 		break
// 	}
// 	console.log("Continue and Break: " +count)
// }

let name = "alexandro"

for(let i = 0; i < name.length; i++) {
	// will print current letters based on its index
	console.log(name[i])

	// if the vowel is equal to a, continue to the next iteration of the loop
	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue
	}
	if(name[i] === "d"){
		break;
	}
}